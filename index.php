<?php

$max_upload = (int) (ini_get('upload_max_filesize'));
$max_post = (int) (ini_get('post_max_size'));
$memory_limit = (int) (ini_get('memory_limit'));
$upload_mb = min($max_upload, $max_post, $memory_limit);

?>

<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Lori Hup Soon</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700,600,800,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,900' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="style.css">
        <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="modal" class="alert fade out alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <i></i>
        </div>
        <div id="site-wrapper">
            <div class="logo-wrapper">
                <a href="#" class="logo"><img src="assets/images/logo.png" alt=""></a>
                <nav class="main-nav">
                    <ul>
                        <li><a id="home-link" href="#">Home</a></li>
                        <li><a id="about-us-link" href="#">About Company</a></li>
                        <li><a id="our-services-link" href="#">Our Services</a></li>
                        <li><a id="our-client-link" href="#">Our Client</a></li>
                        <li><a id="contact-us-link" href="#">Contact Us</a></li>
                    </ul>
                </nav>
            </div>
            <header id="home"> <!-- begin header -->
                <div class="heading wow bounceInLeft">
                    <p>A Healthy Transportation System will satisfy the demand of <br /> your customers hence building a better relationship.</p>
                    <h1>Building Better Ties</h1>
                </div>
                <ul class="slider">
                    <li>
                        <div class="slider-image" style="background-image: url('assets/images/banner-1.jpg')"></div>
                    </li>
                    <li>
                        <div class="slider-image" style="background-image: url('assets/images/banner-2.jpg')"></div>
                    </li>
                    <li>
                        <div class="slider-image" style="background-image: url('assets/images/banner-3.jpg')"></div>
                    </li>
                </ul>
            </header> <!-- end header -->
            <div id="content-wrapper">
                <div id="our-services"> <!-- begin #our-services -->
                    <div class="heading wow bounceInRight">
                        <h1>OUR SERVICES</h1>
                        <p>Specialized Inland distribution service in transporting cargo thorough Malaysia and Singapore.  All our transportation<br/>services commits to 1 day delivery time guarantee.</p>
                    </div>
                    <div class="content">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <div class="item active">
                              <div class="carousel-caption">
                                <ul class="portfolio">
                                    <li class="item">
                                        <img src="assets/images/our-services/slide1/os-icon-1.png" alt="Warehousing">
                                        <h3>Warehousing</h3>
                                        <p>Ample space for Cross docking procedures and Perfect for Short To Long Term leasing</p>
                                    </li>
                                    <li class="item">
                                        <img src="assets/images/our-services/slide1/os-icon-2.png" alt="Cross Border Trucking">
                                        <h3>Cross Border Trucking</h3>
                                        <p>No Hassle, No Mislead , Just Direct Transport Everyday. Orders are synchronize according to time and distance for maximum time efficiency</p>
                                    </li>
                                    <li class="item">
                                        <img src="assets/images/our-services/slide1/os-icon-3.png" alt="Custom brokerage ">
                                        <h3>Custom brokerage</h3>
                                        <p>We got you covered with our 24 hours Custom Clearance Simple Custom Documents & Procurement</p>
                                    </li>
                                    <li class="item">
                                        <img src="assets/images/our-services/slide1/os-icon-4.png" alt="Freight Forwarding">
                                        <h3>Freight Forwarding </h3>
                                        <p>We have a wide affiliates of logistics network. Our forwarders are a proud associates of the Federation of Malaysian Freight Forwarders (FMFF)</p>
                                    </li>
                                </ul>
                              </div>
                            </div>
                            <div class="item">
                              <div class="carousel-caption">
                                <ul class="portfolio">
                                    <li class="item">
                                        <img src="assets/images/our-services/slide2/os-icon-5.png" alt="Lorry Leasing">
                                        <h3>Lorry Leasing</h3>
                                        <p>We have lorry leasing opportunities for companies who does not want an expensive investment</p>
                                    </li>
                                    <li class="item">
                                        <img src="assets/images/our-services/slide2/os-icon-6.png" alt="Additional Guarantee">
                                        <h3>Additional Guarantee</h3>
                                        <p>For your first shipment with Lori Hup Soon Sdn Bhd, If you are not satisfy with our service we can refund the freight charges back to you excluding other miscellaneous fees. (Custom Clearance, GST & Import Duties) *Terms & condition applied*</p>
                                    </li>
                                </ul>
                              </div>
                            </div>

                          </div>
                        </div>


                    </div>
                </div> <!-- end #our-services -->
                <div id="outsource"> <!-- begin #outsource -->
                    <div class="heading wow bounceInLeft">
                        <h1>When to outsource</h1>
                        <p>Specialized Inland distribution service in transporting <br />cargo thorough Malaysia and Singapore.  All our transportation services commits to 1 day delivery time guarantee.</p>
                    </div>
                    <div class="content">
                        <ul class="portfolio wow fadeInUp">
                            <li class="item">
                                <img src="assets/images/out-source/wto-icon-1.png" alt="Reduce overhead cost">
                                <h3>Reduce overhead cost</h3>
                                <p>The cost of maintaining truck fleets, insurance and additional employee payrolls will eat into your profits.</p>
                            </li>
                            <li class="item">
                                <img src="assets/images/out-source/wto-icon-2.png" alt="Focus on targets">
                                <h3>Focus on targets</h3>
                                <p>Let us handle the gritty details while you focus on what you do best, expanding your business</p>
                            </li>
                            <li class="item">
                                <img src="assets/images/out-source/wto-icon-3.png" alt="Expertise Service">
                                <h3>Expertise Service</h3>
                                <p>You can leverage on our dedicated service route with a fraction of the cost of acquiring one yourself. FTL & LTL are available to your needs</p>
                            </li>
                            <li class="item">
                                <img src="assets/images/out-source/wto-icon-4.png" alt="Customer Support">
                                <h3>Customer Support</h3>
                                <p>Rejected Cargos, Import & Export Duties, Procuring Delivery Documents is all in a days work for us. We are always ready to lend a helping hand.</p>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end #oursource -->
                <div id="about-us"> <!-- begin #about-us -->
                    <div class="heading wow bounceInLeft">
                        <h1>About <span class="black">Lori Hup Soon</span></h1>
                    </div>
                    <div class="content wow fadeInUp">
                        <div class="content-inner">
                            <h3 class="title">Our Story</h3>
                            <div class="col-2">
                                <p>Founded by Mr Yeo Kia Siong on 1974, he saw the need to cater to a every growing demand for cross border trucking (Malaysia – Singapore). He has single handedly transform a humble business of 1 truck to now serving more than 200 company's and counting.  He believes that transportation is not only a means of getting the goods from point A to point B but as a means of connecting business and people together as evidence of our 40 decades support from our customers.</p>
                            </div>
                            <div class="col-2">
                                <p><strong>Motto</strong> : Building Better Ties – A Healthy Transportation System will satisfy the demand of your customers thus building a better relationship. </p><p><strong>“Even the most innovative product is useless unless it gets to the hands of the user.” – Anonymous </strong></p>
                            </div>
                        </div>
                        <div class="content-inner">
                            <h3 class="title">How We Work</h3>
                            <div class="col-2">
                                <p>
                                Only 3 steps that get your cargo to your customer.</p>

                                <p>All orders are organized FEW DAYS AHEAD according to their route and distance to achieve cost efficiency for our clients and also ourselves.</p>

                                <p>We have a policy to always help our customer the best we can because even if we cannot provide all the solutions in the world, at least we make you satisfy with our service</p>

                                <p>All of our Drivers practices a delivery time of 1 Day to Singapore and Malaysia. So we really mean what we say about being a dedicate transport provider.</p>

                                <p>Far too many companies has often overlook the most important aspect of their products and services – ensuring the goods arrive on time with the right quantity.</p>

                                <p>We manage a part of your supply chain. We achieve this by Looking at the 3 essentials ingredients: Assets – Process - Information</p>
                            </div>
                            <div class="col-2">
                                <p><strong>What is LOGISTICS</strong> – the movement of goods from the origin to its destination through a series of chain of supplies. 1 in flexibility. </p>

                                <p>Business logistics has already evolve from the simple carriage from point to point to complex internetworks of globalized supplied chains. This is where we will cut through the fog & give you the insights to reduce inefficiency and dead inventory.</p>

                                <p>Successful business such as Wal-Mart & Dell Computers understood the important of a solid logistic foundation. Successful Deliveries = Satisfying Customers.</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- end #about-us -->
                <div id="our-client">
                    <div class="heading wow bounceInRight">
                        <h1>Our <span class="black">Client</span></h1>
                        <p>Our customers have TRUST us to BUILT their business tremendously. Question is are you on board ?</p>
                    </div>
                    <div class="content wow fadeInUp">
                        <img src="assets/images/our-clients/oc-1.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-2.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-3.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-4.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-5.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-6.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-7.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-8.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-9.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-10.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-11.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-12.jpg" alt="" class="client-logo">
                        <img src="assets/images/our-clients/oc-13.jpg" alt="" class="client-logo">
                    </div>
                </div>
                <div id="customers"> <!-- begin #customers-saying-about-us -->
                    <div class="heading wow bounceInLeft">
                        <h1>Customers <span class="white">saying about us</span></h1>
                        <p>We have always rely on our customers honest feedback as you are always encouraging us to put in ‘the extra mile’ in our <br /> ourselves to serve you better. Over 200 customers and counting has enjoy our services for more than 40 years. Find out <br /> why they have stick by us for all these times !</p>
                    </div>
                    <div class="content wow fadeInUp">
                        <div class="content-inner">
                            <div class="col-4">
                                <div class="saying">
                                    <p>“Glad to find a good & reliable transport company like yours more than 20 years ago. Thank you for the great working relationship.”</p>
                                    <p class="who"><strong><em>PH Tan, General Manager <br />APPROFIT CHEMICALS SDN BHD</em></strong></p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="saying">
                                    <p>“We have our fair share of crisis due to unforseen circumstances but these hindrances have always further strengthened our partnership through principles of cooperation, mutual trust and respect. Mr Yeo you are the model of excellence to your industry and profession and the quality of your staff and service level is a reflection of your leadership.”</p>
                                    <p class="who"><strong><em>Wong Fook Choon, Director <br />TRANSASIA FINE PAPERS (M) SDN BHD</em></strong></p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="saying">
                                    <p>“Hup Soon transport has achieve our Transporter Performance rating for 2014,  90 out of 100 possible points. We hope that the above will be a guide to your company in providing us better services in near furture.”</p>
                                    <p class="who"><strong><em>Haji Omar, Logistic Department Head <br />FEC CABLES (M) SDN BHD</em></strong></p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="saying">
                                    <p>“Biz partnership for 40 years !  It’s amazing and hard to come by ! To maintain such cordial working relationship, the recipe is a lot of patience/tolerance/mutual respect/understanding/professionalism We appreciate your valued supports and wish your biz will grow to another height. Thank you.”</p>
                                    <p class="who"><strong><em>Corinne Teoh, Logistics Manager <br />NYLEX MALAYSIA BERHAD</em></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end #customers-saying-about-us -->
                <div id="logistics-network"> <!-- begin #logistics-network -->
                   <div class="heading wow bounceInRight">
                       <h1>Our <span class="black thin">logistics network </span></h1>
                       <p>This map includes are service route in Both Countries, Malaysia & Singapore <br /> Daily trips to Singapore and Malaysia</p>
                   </div>
                   <div class="content">
                        <img src="assets/images/logistics-network/path-1.png" alt="" class="path path-1 wow flash" data-wow-duration="2s" data-wow-delay="1s" data-wow-iteration="infinite">
                        <img src="assets/images/logistics-network/path-2.png" alt="" class="path path-2 wow flash" data-wow-duration="2s" data-wow-delay="1s" data-wow-iteration="infinite">
                        <div class="sub-heading wow bounceInLeft">
                            <p>A Healthy Transportation System will satisfy the demand of <br />your customers hence building a better relationship.</p>
                            <h3>Building Better Ties </h3>
                        </div>
                   </div>
                </div> <!-- end #logistics-network -->
                <div id="our-careers"> <!-- begin #our-careers -->
                    <div class="heading wow bounceInRight">
                        <h1>Our <span class="black thin">Careers</span></h1>
                        <p>We are currently looking for awesome candidates to join our awesome team to go on our awesome mission together.</p>
                    </div>
                    <div class="content">
                        <p class="welcome wow fadeInUp">“Hire people who are better than you are, then leave them to get on with it . . . ; <br /> Look for people who will aim for the remarkable, who will not settle for the routine.” <br /> <span class="thin">– David Ogilvy -</span></p>
                        <ul class="portfolio wow fadeInUp">
                            <li class="item">
                                <h3>Get paid<br /> learning<br />the things<br /> you want</h3>
                                <p>Here at Hup Soon, we believe personal growth is important to your overall wellbeing as many of us wants to reach our peak self.  Just consult us regarding the area you want to improve on and we will fund for your training or books you want.</p>
                            </li>
                            <li class="item">
                                <br/><h3>Complimentary<br />Perks</h3>
                                <p>You've got to ask yourself a question: 'do I feel lucky?' Well, do ya, punk? – Dirty Harry, Clint Eastwood. <br />We love movies as much as you do that’s why we encourage our staffs to enjoy a night out with their favorite movies. This is just one of the many perks we offer in our company. *Please leave your favorite movie list in your resume</p>
                            </li>
                            <li class="item">
                                <h3>An Open<br />Working<br />Culture</h3>
                                <p>I am sure we have all experience some kind of abuse from the traditional hierarchy work structure. Hup Soon was built upon a foundation of open culture of different ethnics and races. We believe in a work culture that promotes open communication hence the lack of borders in our office.</p>
                            </li>
                        </ul>
                        <p><em>We are currently hiring these job positions :</em></p>
                        <div class="panel-group wow flipInX" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default active">
                            <div class="panel-heading" role="tab" id="headingOne">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  Logistic admin assistant
                                </a>
                              </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                Minimum SPM requirement <br />
                                Training will be provided
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  Lorry Driver outbound from Malaysia to Singapore
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="panel-body">
                                Must possess GDL licence for 9 m/t and above <br />
                                Have experience in this industry for at least 2 years
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div> <!-- end #our-careers -->
                <div id="contact-us">
                    <div class="heading wow bounceInRight">
                        <h1>Get In Touch <span class="black thin">With Us</span></h1>
                        <p>Your Enquiry, Our Service. Welcom to Drop Us a message</p>
                    </div>
                    <div class="content wow fadeInUp">
                        <form enctype="multipart/form-data" id="contact-form">
                            <p>
                                <input type="text" name="name" id="name" placeholder="Your Name" required>
                                <input type="email" name="email" id="email" placeholder="Email" required>
                            </p>
                            <p>
                                <input type="text" name="tel" id="tel" placeholder="Phone Number" required>
                                <input type="text" name="company" id="company" placeholder="Company Name">
                            </p>
                            <p>
                                <textarea name="message" id="message" rows="10" placeholder="Your needs" required></textarea>
                            </p>
                            <div class="upload-wrapper">
                                <input type="text" role="button" disabled="disabled" placeholder="Attachments" id="uploadFile" data-toggle="popover" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?">
                                <div class="file-upload btn">
                                    <span>Upload</span>
                                    <input type="file" name="file" id="file" class="upload" >
                                    <input type="hidden" value="<?php echo $upload_mb;?>" id="upload-size">
                                </div>
                            </div>
                            <p><img src="assets/images/ajax-loader.gif" class="loader" alt=""><input type="submit" name="submit" id="submit" value="Send Message"></p>
                        </form>
                    </div>
                </div>
            </div>
            <footer> <!-- begin footer -->
                <p class="copyright wow fadeInUp">© 2014 Syarikat Lori Hup Soon Sdn bhd  - All Rights Reserved</p>
            </footer> <!-- end footer -->
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.2/ScrollMagic.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.2/plugins/debug.addIndicators.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>
