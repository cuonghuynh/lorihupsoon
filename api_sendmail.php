<?php 

require 'api/sendmail.php';
require 'api/uploader.php';


if (is_ajax()) {

	$errors = array();
	$oldInput = array();

	if ( !empty($_POST) ) {

		//validate form
		$name = $_POST['name'];
		$email = $_POST['email'];
		$company = $_POST['company'];
		$tel = $_POST['tel'];
		$message = $_POST['message'];
		$attachment = '';

		if ( empty($name) ) {
			$errors['name'] = 1;
		} else {
			$oldInput['name'] = $name;
		}

		if ( empty($email) ) {
			$errors['email'] = 1;
		} else {
			$oldInput['email'] = $email;
		}

		if ( empty($company) ) {
			$errors['company'] = 1;
		} else {
			$oldInput['company'] = $company;
		}

		if ( empty($tel) ) {
			$errors['tel'] = 1;
		} else {
			$oldInput['tel'] = $tel;
		}

		if ( empty($message) ) {
			$errors['message'] = 1;
		} else {
			$oldInput['message'] = $message;
		}

		if ( !empty($_FILES['file']['name']) ) {

			$uploader = new Uploader($_FILES['file']['tmp_name']);
			$uploader->setUploadsDir((dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'uploads');
			if ($uploader->upload($_FILES['file']['name']) )
			{
				$attachment = $uploader->getUploadedFilePath();
			} else {
				echo json_encode('Sorry, there was an error uploading your file.');
				return false;
			}
		}

		if ( count($errors) == 0) {

			$mail = new Mail( 587 ,
				'smtp.gmail.com', 
				'tls', 
				'lorihupsoon.webmaster@gmail.com', 
				'noospuhirol', 
				'Lori Hup Soon Enquiry Form', 
				'orders@lorihupsoon.com.my', 
				'Lori Hup Soon' );

			if ( !$mail->send('Contact Form', $name, $email, $company, $tel, $message, $attachment) ) {
				echo json_encode($mail->errors);
			} else {
				echo json_encode('true');
			}

		} else {
			echo json_encode($oldInput);
		}

	}

}


//Function to check if the request is an AJAX request
function is_ajax() {
  	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

 ?>