$(document).ready(function() {

	new WOW().init();
	initSlider();
	scrolling();
	customCollapse();
	uploadingFile();
	initScrollMagic();

	$('.main-nav > ul > li > a').click(function (e) {
		e.preventDefault();
		goToByScroll($(this).attr('id'));
	});

	$('#contact-form').submit(function(event) {
		event.preventDefault();

		$('.loader').css('display', 'initial');
		$('#submit').prop('disabled', true).css('cursor', 'not-allowed');


		var dt = new FormData(this);
		
		$.ajax({
			url: 'api_sendmail.php',
			type: 'POST',
			dataType: 'json',
			contentType: false,
   			processData: false,
			data: dt,
		})

		.done(function(res) {
			if (res != 'true') {
				showModal('alert-warning', res);
			} else {
				showModal('alert-success', 'Enquiry email sent.');
			}

			$('.loader').css('display', 'none');
			$('#submit').prop('disabled', false).css('cursor', 'pointer');
		})
		.fail(function(jqXHR, textStatus, errorThrown) {

			showModal('alert-danger', "The following error occurred: "+ textStatus, errorThrown );

			$('.loader').css('display', 'none');
			$('#submit').prop('disabled', false).css('cursor', 'pointer');
		});
		
	});

});

function initScrollMagic()
{
	var controller = new ScrollMagic.Controller();

	var h = $('#home').height();
	new ScrollMagic.Scene({triggerElement: '#home', duration: h})
						.setClassToggle('#home-link', 'active')
						.addTo(controller);

	h = $('#about-us').height();					
	new ScrollMagic.Scene({triggerElement: '#about-us', duration: h})
						.setClassToggle('#about-us-link', 'active')
						.addTo(controller);

	h = $('#our-services').height();
	new ScrollMagic.Scene({triggerElement: '#our-services', duration: h})
						.setClassToggle('#our-services-link', 'active')
						.addTo(controller);

	h = $('#our-client').height();
	new ScrollMagic.Scene({triggerElement: '#our-client', duration: h})
						.setClassToggle('#our-client-link', 'active')
						.addTo(controller);

	h = $('#contact-us').height();
	new ScrollMagic.Scene({triggerElement: '#contact-us', duration: h})
						.setClassToggle('#contact-us-link', 'active')
						.addTo(controller);

}

function goToByScroll(id)
{
	id = id.replace('-link', '');
	$('html, body').animate({scrollTop: $('#' + id).offset().top - 70}, 400);
}

function uploadingFile()
{
	$('.upload').change(function (e) {
		e.preventDefault();

		$('#uploadFile').val($(this).val());
		var ext = ['zip','rar'];
		var uploadSize = $('#upload-size').val();

		$('#uploadFile').removeClass('animated shake');
		$('#modal').removeClass('animated fadeInDown show');

		if ($.inArray($(this).val().split('.').pop().toLowerCase(), ext) == -1) {
			$('#uploadFile').addClass('animated shake');
			$('#uploadFile').val('');

			showModal('alert-warning', 'File must be .zip | .rar');

		} else {

			if ( this.files[0].size > (uploadSize * 1024 * 1024) ) {

				showModal('alert-danger', 'File must be ZIP or RAR, less than ' + uploadSize + ' MB');

			} else {

				$('#uploadFile').val($(this).val().split('\\').pop());
			}
		}
	});
}

function showModal(alertType, content)
{
	$('#modal').removeClass('alert-warning');
	$('#modal').removeClass('alert-danger');
	$('#modal').removeClass('alert-success');
	$('#modal i').html(content);
	$('#modal').addClass(alertType + ' animated fadeInDown show');
	setTimeout(function(){
		$('#modal').removeClass('animated fadeInDown show');
	}, 5000);
}

function customCollapse()
{

	$('.panel-title>a').click(function (e) {
		$('#accordion').find('.panel.panel-default.active').removeClass('active');
		e.preventDefault();
		$(this).closest('.panel.panel-default').addClass('active');

	});

}

function scrolling()
{
	//calculating menu position 
	$(window).scroll(function(event) {
		if ($(this).scrollTop() > 10) {
			$('.logo-wrapper').addClass('fixed');
		} else {
			$('.logo-wrapper').removeClass('fixed');
		}
	});
}

function initSlider()
{
	var _current = 1;
	var _next = 1;
	$("ul.slider > li:gt(0)").css('opacity', 0);

	setInterval(function() {
		
		_next = _current + 1;

		if (_next > $('ul.slider li').length) {
			_next = 1;
		};

		_str = "ul.slider li:nth-child(" + _next + ")";
		$(_str).animate({opacity: 1.0}, 2000);
		_str = "ul.slider li:nth-child(" + _current + ")";
		$(_str).animate({opacity: 0.0}, 2000);

		_current = _next;

	}, 7000);

}
