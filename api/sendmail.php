<?php 

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require 'phpMAILER/PHPMailerAutoload.php';

class Mail
{

	protected $port 			= 587;
	protected $host 			= 'smtp.gmail.com';
	protected $secureMode		= 'tls';

	protected $sysEmail 		= 'lorihupsoon.webmaster@gmail.com';
	protected $sysEmailPass 	= 'noospuhirol';
	protected $sysName			= 'Lori Hup Soon';
	protected $recipEmail 		= '';
	protected $recipName		= '';

	protected $mail;

	public $errors;

	/**
	 * Contruct SendMail oject
	 * @param [int] $port        
	 * @param [string] $host         	Sender Email's Host
	 * @param [string] $secureMode   	Secure Mode
	 * @param [string] $sysEmail     	Using this email to send to another
	 * @param [string] $sysEmailPass 	System email password
	 * @param [string] $sysName 	 	System Email Name
	 * @param [string] $recipEmail   	Receiver email
	 * @param [string] $recipName    	Receiver name
	 */
	public function Mail($port, $host, $secureMode, $sysEmail, $sysEmailPass, $sysName , $recipEmail, $recipName)
	{
		$this->port 			= $port;
		$this->host 			= $host;
		$this->secureMode		= $secureMode;
		$this->sysEmail 		= $sysEmail;
		$this->sysEmailPass		= $sysEmailPass;
		$this->sysName 			= $sysName;
		$this->recipEmail 		= $recipEmail;
		$this->recipName 		= $recipName;

		$this->initMail();
	}


	protected function initMail()
	{
		//Create a new PHPMailer instance
	    $this->mail = new PHPMailer;
	    //Tell PHPMailer to use SMTP
	    $this->mail->isSMTP();
	    $this->mail->CharSet = 'UTF-8';
	    //Enable SMTP debugging
	    // 0 = off (for production use)
	    // 1 = client messages
	    // 2 = client and server messages
	    $this->mail->SMTPDebug = 0;
	    //Ask for HTML-friendly debug output
	    $this->mail->Debugoutput = 'html';
	    //Set the hostname of the mail server
	    $this->mail->Host = $this->host;
	    //Set the SMTP port number - likely to be 25, 465 or 587
	    $this->mail->Port = $this->port;
	    //Whether to use SMTP authentication
	    $this->mail->SMTPAuth = true;
	    $this->mail->SMTPSecure = $this->secureMode;
	    //Username to use for SMTP authentication
	    $this->mail->Username = $this->sysEmail;
	    //Password to use for SMTP authentication
	    $this->mail->Password = $this->sysEmailPass;
	    //Set who the message is to be sent from
	    $this->mail->setFrom($this->sysEmail, $this->sysName);
	    //Set an alternative reply-to address
	    $this->mail->addReplyTo($this->recipEmail, $this->recipName);
	    //Set who the message is to be sent to
	    $this->mail->addAddress($this->recipEmail, $this->recipName);
	    //$this->$mail->addAddress($email, $name);
	}

	public function send($subject, $name, $email, $company, $contact, $message, $attachment)
	{

		//Set the subject line
	    $this->mail->Subject = $subject;
	    //Read an HTML message body from an external file, convert referenced images to embedded,
	    //convert HTML into a basic plain-text alternative body
	    //$this->$mail->msgHTML(file_get_contents('phpMAILER/contents.php'), dirname(__FILE__));
	    //Replace the plain text body with one created manually
	    $this->mail->Body = 	"<div style='width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;''>";
	    $this->mail->Body	.= 	"<h1>Contact Form</h1>";
	    $this->mail->Body	.=	"<p>Customer's Infomation :</p>";
	    $this->mail->Body	.=	"<p>Name : ". $name ."</p>";
	    $this->mail->Body	.=	"<p>Email : ". $email ."</p>";
	    $this->mail->Body	.=	"<p>Contact No. : ". $contact ."</p>";
	    $this->mail->Body	.=	"<p>Company : ". $company ."</p>";
	    $this->mail->Body	.=	"<p><strong>Content : </strong></p>";
	    $this->mail->Body	.=	"<p>". $message ."</p>";
	    $this->mail->Body	.=  "</div>";
	    
	    $fileName = basename($attachment);

	    if ($attachment != '') {
	    	$this->mail->AddAttachment( $attachment , $fileName);
	    }
	    
	    //Attach an image file
	    // $this->$mail->addAttachment('images/phpmailer_mini.png');
	    $this->mail->IsHTML(true);
	    //send the message, check for errors
	    if (!$this->mail->send()) {
	       
	        // echo "Mailer Error: " . $this->$mail->ErrorInfo;
	        $this->errors = $this->mail->ErrorInfo;;
	        return false;
	    } else {
	        return true;
	        //echo "Message sent!";
	    }
	}
    
    
}

?>