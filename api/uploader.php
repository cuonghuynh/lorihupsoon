<?php 

class Uploader
{
	protected $uploadsDir;
	protected $uploadsDirName;
	protected $fileName;
	protected $temp;

	public function Uploader( $tmp )
	{
		$this->temp = $tmp;
	}

	public function setUploadsDir( $path )
	{
		$this->uploadsDir = $path;
		$this->uploadsDirName = basename($path);
	}

	public function upload( $fileName )
	{
		$this->fileName = $fileName;
		return move_uploaded_file($this->temp, $this->uploadsDir . DIRECTORY_SEPARATOR . $fileName) ;
	}

	public function getUploadedFilePath()
	{
		return $this->uploadsDir . DIRECTORY_SEPARATOR . $this->fileName;
	}

}


?>